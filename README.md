# Tips Singularity

## 0. Create your Singularity image with a definition file (including [Theia IDE](https://theia-ide.org/))
This method requires to pull images from Dockerhub but [this might not last](https://blog.alexellis.io/docker-is-deleting-open-source-images/).
A [definition file](https://docs.sylabs.io/guides/latest/user-guide/definition_files.html) is basically the same as a Dockerfile. 

Install the [singularity container package](https://docs.sylabs.io/guides/latest/user-guide/). This package should be available on your favorite distribution.

You only need access to a cpu to build your image.
```sh
ssh ${USER}@igrida-frontend.irisa.fr
```
Then
```sh
oarsub -I -l "walltime=1:00:00"
```
You can customise your Python env in the requirements.txt file.
I usually place my images in ```/srv/longdd/${USER}/``` with the corresponding definition files in ```/srv/longdd/${USER}/container_def/```
```sh
./build_img.sh /path/to/your_image.sif /path/to/your_image.def
```

## 1. Run [Theia IDE](https://theia-ide.org/) in your container
The package.json file contains the blueprint for your IDE.
You can use the latest VSCode plugins found on [OpenVSX](https://open-vsx.org/) as long as Theia supports them (it should be up to the latest VSCode API by now).

After running the script, you can find the http address in the terminal output.
By default, Theia uses the port 6006.
```sh
./run_theia.sh /path/to/your_image.sif
```

## 2. Spawn a new shell within your container
```sh
./run_shell.sh /path/to/your_image.sif
```
## 3. Run a passive job in your container
```sh
oarsub -S "./run_passive.sh path/to/your_script.py"
```
