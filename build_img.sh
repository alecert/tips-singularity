#!/bin/bash
module load spack/gvirt
gvirt start sing --image /srv/soft/gvirt-images/alpine-3.12.1-singularity-x86_64.qcow2
gvirt exec sing "export SINGULARITY_CACHEDIR=/mnt/srv/tempdd/$USER/singularity; singularity build -F /mnt/srv/longdd/$USER/$1 /mnt/srv/longdd/$USER/container_def/$2"
gvirt exec sing /sbin/poweroff
