#!/bin/sh
#OAR -l {(host ='igrida-abacus20.irisa.fr' AND os='debian10')}/gpu_device=1,walltime=1:00:00
#OAR -O /srv/tempdd/alecert/SCRATCH/%jobid%.output
#OAR -E /srv/tempdd/alecert/SCRATCH/%jobid%.error

. /etc/profile.d/modules.sh

set -xv

echo
echo OAR_WORKDIR : $OAR_WORKDIR
echo
echo "cat \$OAR_NODE_FILE :"
cat $OAR_NODE_FILE
echo

echo
echo "=============== RUN ==============="

nvidia-smi
module load singularity/3.8.5
singularity exec --nv -B /srv/longdd/$USER,/srv/tempdd/$USER /srv/longdd/$USER/theia.sif python "$@"

echo "=============== DONE =============="
