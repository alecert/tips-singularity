#!/bin/bash
module load singularity/3.8.5
singularity instance start --nv -B /srv/longdd/$USER,/srv/tempdd/$USER $1 container
singularity shell instance://container
singularity instance stop --all
