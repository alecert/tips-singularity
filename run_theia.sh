#!/bin/bash
module load singularity/3.8.5
singularity exec --nv -B /srv/longdd/$USER,/srv/tempdd/$USER $1 yarn --cwd /opt/ide theia start --hostname $(hostname -I)
